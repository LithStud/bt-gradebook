<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lecture;
use Purifier;

class LectureController extends Controller
{
    public function index(Request $request, Lecture $lecture)
    {
        $searchFilter = $request->input('search_term', session()->get('lecture_search_term', ''));
        $searchFilter = strip_tags($searchFilter);

        session()->put('lecture_search_term', $searchFilter);

        $lectures = Lecture::when($searchFilter, function ($q) use ($searchFilter) {
            $q->where('name', 'LIKE', "%$searchFilter%");
        })->orderBy('name', 'ASC')->paginate();

        return view('lectures.index', compact('lectures', 'searchFilter'));
    }

    public function add()
    {
        $edit = false;
        return view('lectures.add_edit', compact('edit'));
    }

    public function edit($id, Lecture $lecture)
    {
        $lecture = $lecture->findOrFail($id);
        $edit = true;
        return view('lectures.add_edit', compact('edit', 'lecture'));
    }

    public function save($id = null, Request $request, Lecture $lecture)
    {
        $validated = $request->validate([
            'name' => 'required|string|max:128',
            'description' => 'nullable|string',
        ]);

        if ($validated) {
            $validated['name'] = strip_tags($validated['name']);
            $validated['description'] = Purifier::clean($validated['description']);
            $lecture->updateOrCreate(['id' => $id], $validated);
            return redirect(route('lectures'));
        }

        return redirect()->back()->withInput();
    }

    public function delete(Request $request, Lecture $lecture)
    {
        $delete = $request->input('delete', []);

        if ($delete) {
            $lecture->destroy($delete);

            $searchFilter = session()->get('lecture_search_term', '');

            $filterCount = $lecture->where('name', 'LIKE', "%$searchFilter%")
                ->count();


            // reset filter if there is nothing to show
            if($filterCount < 1) {
                session()->put('lecture_search_term', '');
                return redirect(route('lectures'));
            }
        }
        //dump($delete);
        return redirect()->back();
    }
}
