<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

class StudentController extends Controller
{
    public function index(Request $request, Student $student)
    {
        $searchFilter = $request->input('search_term', session()->get('student_search_term', ''));
        $searchFilter = strip_tags($searchFilter);

        session()->put('student_search_term', $searchFilter);

        $students = Student::when($searchFilter, function ($q) use ($searchFilter) {
            $q->where('name', 'LIKE', "%$searchFilter%")
                ->orWhere('lastname', 'LIKE', "%$searchFilter%");
        })->orderBy('lastname', 'ASC')->paginate();
        return view('students.index', compact('students', 'searchFilter'));
    }

    public function grades($id, Student $student)
    {
        $stud = $student->find($id);
        $grades = $stud->lectures()->get()->sortBy('name')->groupBy('name');
        return view('students.grades', compact('stud', 'grades'));
    }

    public function add()
    {
        $edit = false;
        return view('students.add_edit', compact('edit'));
    }

    public function edit($id)
    {
        $student = Student::findOrFail($id);
        $edit = true;
        return view('students.add_edit', compact('edit', 'student'));
    }

    public function save($id = null, Request $request, Student $student)
    {
        $validated = $request->validate([
            'name' => 'required|string|max:128',
            'lastname' => 'required|string|max:128',
            'email' => 'required|email|max:128',
            'phone' => 'required|string|max:32',
        ]);

        if ($validated) {
            $validated['name'] = strip_tags($validated['name']);
            $validated['lastname'] = strip_tags($validated['lastname']);
            $validated['email'] = strip_tags($validated['email']);
            $validated['phone'] = strip_tags($validated['phone']);
            $student->updateOrCreate(['id' => $id], $validated);
            return redirect(route('students'));
        }

        return redirect()->back()->withInput();
    }

    public function delete(Request $request, Student $student)
    {
        $delete = $request->input('delete', []);

        if ($delete) {
            $student->destroy($delete);

            $searchFilter = session()->get('student_search_term', '');

            $filterCount = $student->where('name', 'LIKE', "%$searchFilter%")
                ->orWhere('lastname', 'LIKE', "%$searchFilter%")
                ->count();


            // reset filter if there is nothing to show
            if ($filterCount < 1) {
                session()->put('student_search_term', '');
                return redirect(route('students'));
            }
        }
        return redirect()->back();
    }
}
