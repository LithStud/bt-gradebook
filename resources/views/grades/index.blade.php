@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="paginator-container">
                    {{ $students->links() }}
            </div>
            @foreach ($students as $student)
            <div class="card mb-3">
                <div class="card-body">
                    <h5 class="card-title">Student: {{$student->name}} {{$student->lastname}}</h5>
                    <ul class="list-group list-group-flush">
                        @foreach ($student->lectures->sortBy('name')->groupBy('name') as $name => $data)
                        <li class="list-group-item">{{ $name }}
                            <ul class="pagination">
                                @foreach ($data as $lectureInfo)
                                <li class="page-item page-link">{{ $lectureInfo->pivot->grade }}</li>
                                @endforeach
                            </ul>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endforeach
            <div class="paginator-container">
                    {{ $students->links() }}
            </div>
        </div>
    </div>
</div>

@endsection
