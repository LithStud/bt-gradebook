@extends('layouts.app')

@section('content')
<div class="container">
    <div class="jumbotron jumbotron-fluid text-center">
        <div class="container">
            <h1 class="display-4">Project: GradeBook</h1>
            <p class="lead">Welcome to the GradeBook, @auth professor. @else if you are professor please login in order
                to grade students.@endauth</p>
            @auth
            <a class="btn btn-success btn-lg" href="{{ route('students') }}" role="button">Students</a>
            <a class="btn btn-success btn-lg" href="{{ route('lectures') }}" role="button">Lectures</a>
            <a class="btn btn-success btn-lg" href="{{ route('grading') }}" role="button">Grading</a>
            @else
            <a class="btn btn-success btn-lg" href="{{ route('login') }}" role="button">Login</a>
            @endauth
        </div>
    </div>
</div>
@endsection
