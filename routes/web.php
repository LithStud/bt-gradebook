<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('front');
})->name('home');

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::middleware('auth')->group(function () {
    // list views
    Route::get('/grades', 'GradeController@index')->name('grades');
    Route::get('/students', 'StudentController@index')->name('students');
    Route::get('/students/{id}/grades', 'StudentController@grades')->name('students.grades')->where('id', '[0-9]+');
    Route::get('/lectures', 'LectureController@index')->name('lectures');

    // add new views
    Route::get('/students/add', 'StudentController@add')->name('students.add');
    Route::get('/lectures/add', 'LectureController@add')->name('lectures.add');
    // grading view
    Route::get('/grading', 'GradeController@grading')->name('grading');

    // edit view
    Route::get('/students/{id}/edit', 'StudentController@edit')->name('students.edit')->where('id', '[0-9]+');
    Route::get('/lectures/{id}/edit', 'LectureController@edit')->name('lectures.edit')->where('id', '[0-9]+');

    // saving new/edited entry
    Route::post('/students/save', 'StudentController@save')->name('students.save');
    Route::put('/students/{id}/save', 'StudentController@save')->name('students.save.id')->where('id', '[0-9]+');
    Route::post('/lectures/save', 'LectureController@save')->name('lectures.save');
    Route::put('/lectures/{id}/save', 'LectureController@save')->name('lectures.save.id')->where('id', '[0-9]+');
    Route::post('/grades/save', 'GradeController@save')->name('grades.save');

    // deletion routes
    Route::delete('/students/delete', 'StudentController@delete')->name('students.delete');
    Route::delete('/lectures/delete', 'LectureController@delete')->name('lectures.delete');
});
